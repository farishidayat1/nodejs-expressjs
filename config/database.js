import { Sequelize } from "sequelize";
 
const db = new Sequelize('rest-api-expressjs', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

export default db;