/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('products').del()
  await knex('products').insert([
    {
      id: 1, 
      name: 'product 1',
      qty: 5,
      picture: "http://example.com/burton.jpg",
      expiredAt: "2022-08-17",
      isActive: true
    }
  ]);
};
