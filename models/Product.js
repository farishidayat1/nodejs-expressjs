import { Sequelize } from "sequelize";
import db from "../config/database.js";
 
const { DataTypes } = Sequelize;
 
const Product = db.define('products', { 
  name: {
    type: DataTypes.STRING
  },
  qty: {
    type: DataTypes.INTEGER
  },
  picture: {
    type: DataTypes.TEXT
  },
  expiredAt: {
    type: DataTypes.DATE
  },
  isActive: {
    type: DataTypes.BOOLEAN
  },
}, {
  tableName: 'products'
})
 
export default Product;